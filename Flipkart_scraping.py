# importing the libraries 
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

# URL required to parse
my_url= 'https://www.flipkart.com/mobiles/~redmi-note-4/pr?sid=tyy,4io'

# Opening and reading the URL
uClient = uReq(my_url)
page_html= uClient.read()
uClient.close()

# Soup object
page_soup= soup(page_html, "html.parser")

# Fetching the data
containers= page_soup.findAll("div",{"class":"_1-2Iqu row"})

# File handling
file_name="Flipkart_Products.csv"
f=open(file_name,"w")

headers = "Product_Name, Price, Rating, User_Rated, User_Reviewed\n"

f.write(headers)

for container in containers:
#container=containers[0]
    
    product_name=container.div.div.text
    print(product_name)
    
    price_container=container.findAll("div",{"class":"_1vC4OE _2rQ-NK"})
    price = price_container[0].text
    print(price[1:]) 
    
    rating_container=container.findAll("div",{"class":"hGSR34 _2beYZw"})
    rating = rating_container[0].text
    print(rating[:-2])
    
    r_and_r_container=container.findAll("span",{"class":"_38sUEc"})
    r_and_r = r_and_r_container[0].span.text
    list1=r_and_r.split("\xa0&\xa0")
    no_of_ratings=list1[0][:-8]
    print(no_of_ratings)
    no_of_reviews=list1[1][:-8]
    print(no_of_reviews)
    print(product_name.replace(",","-")+','+ price[1:].replace(",","")+ ',' +rating[:-2].replace(",","")+','+no_of_ratings.replace(",","")+','+no_of_reviews.replace(",","")+'\n')
    f.write(product_name.replace(",","-")+','+ price[1:].replace(",","")+ ',' +rating[:-2].replace(",","")+','+no_of_ratings.replace(",","")+','+no_of_reviews.replace(",","")+'\n')
    
    print()
    
f.close()